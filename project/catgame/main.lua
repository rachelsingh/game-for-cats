textures = {
	cat = Texture.new( "cat.png" ),
	bird = Texture.new( "bird.png" ),
	mouse = Texture.new( "mouse.png" ),
	bgTiles = Texture.new( "tiles.png" ),
	bgSnow = Texture.new( "snowy.png" ),
	bgRoom = Texture.new( "room.png" ),
	bgGrass = Texture.new( "grassy.png" ),
	bgDesert = Texture.new( "desert.png" )
}

sounds = {
	bird = Sound.new( "bird.mp3" ),
	mouse = Sound.new( "mouse.wav" ),
	cat = Sound.new( "cat.wav" )
}

bgCounter = 0

function SetupGame()
	background = Bitmap.new( textures.bgDesert )
	stage:addChild( background )

	cat 		= Character.new()
	cat:Setup( { name = "cat", texture = textures.cat, speed = 4, sound = sounds.cat, timerSpeed = 1 } )
	cat:Draw()
	
	mouse	= Character.new()
	mouse:Setup( { name = "mouse", texture = textures.mouse, speed = 4, sound = sounds.mouse, timerSpeed = 1 } )
	mouse:Draw()
	
	bird 		= Character.new()
	bird:Setup( { name = "bird", texture = textures.bird, speed = 4, sound = sounds.bird, timerSpeed = 1 } )
	bird:Draw()

	stage:addEventListener( Event.MOUSE_DOWN, MouseDown, self )
	stage:addEventListener( Event.ENTER_FRAME, EnterFrame, self )
end

function UpdateBackground()
	bgCounter = bgCounter + 1
	if ( bgCounter == 1000 ) then
		local bg = math.random( 0, 5 )
		
		if ( bg == 1 ) then		background:setTexture( textures.bgTiles ) 		
		elseif ( bg == 2 ) then		background:setTexture( textures.bgSnow ) 		
		elseif ( bg == 3 ) then		background:setTexture( textures.bgRoom ) 		
		elseif ( bg == 4 ) then		background:setTexture( textures.bgGrass ) 		
		elseif ( bg == 5 ) then		background:setTexture( textures.bgDesert ) 		end
		
		bgCounter = 0
	end
end

function EnterFrame( event )
	bird:Update()
	cat:Update()
	mouse:Update()
	UpdateBackground()
end

function MouseDown( event )
	if ( cat.image:hitTestPoint( event.x, event.y ) ) then
		cat:Clicked()
	elseif ( bird.image:hitTestPoint( event.x, event.y ) ) then
		bird:Clicked()
	elseif ( mouse.image:hitTestPoint( event.x, event.y ) ) then
		mouse:Clicked()
	end
end

SetupGame()

