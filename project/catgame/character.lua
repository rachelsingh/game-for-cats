Character = Core.class( sprite )

function Character:init()
	self.behavior = ""
	self.timer = 0
	self.angle = 0
	self.speed = 0
	self.image = nil
	self.name = ""
	self.timerSpeed = 0
	self.speed = 0
	self.sound = nil
	self.xVel = 0
	self.yVel = 0
	self.acc = 0.2
	self.deacc = 0.2
end

function Character:Draw()
	stage:addChild( self.image )
end

function Character:RandomAngle()
	local rand = math.random( 0, 4 )
	
	if ( rand == 0 ) then
		return 0
	elseif ( rand == 1 ) then
		return 90
	elseif ( rand == 2 ) then
		return 180
	else
		return 270
	end
end

function Character:RandomBehavior()
	local i = math.random( 0, 2 )
	
	behavior = "still"
	if ( i == 0 ) then
		return "walk"
	elseif ( i == 1 ) then
		return "run"
	elseif ( i == 2 ) then
		return "disappear"
	elseif ( i == 3 ) then
		return "random"
	end
	
	return behavior
end

function Character:RandomPosition()
	return math.random( 0, 620 ), math.random( 0, 340 )
end

function Character:Clicked()
	self.behavior = "disappear"
end

function Character:MoveForward( angle, speed )
	if ( speed == nil ) then speed = 1 end
	
	if ( angle == 0 ) then
		self.xVel = self.xVel - self.acc
	elseif ( angle == 90 ) then
		self.yVel = self.yVel - self.acc
	elseif ( angle == 180 ) then
		self.xVel = self.xVel + self.acc
	else
		self.yVel = self.yVel + self.acc
	end
end

function Character:MoveRandom( x, y, speed )
	x = x + math.random( -10, 10 )
	y = y + math.random( -10, 10 )
end

function Character:Physics( x, y )
	x = x + self.xVel
	y = y + self.yVel
	return x, y
end

function Character:Deaccelerate()
	if ( self.xVel < 0 ) then
		self.xVel = self.xVel + self.deacc
	elseif ( self.xVel > 0 ) then
		self.xVel = self.xVel - self.deacc	
	end
	
	if ( self.yVel < 0 ) then
		self.yVel = self.yVel + self.deacc	
	elseif ( self.yVel > 0 ) then
		self.yVel = self.yVel - self.deacc		
	end
end

function Character:StayOnScreen( x, y )
	if ( x < 0 ) then 
		self.xVel = 0
		x = 0 
	end
	if ( x > 620 ) then
		self.xVel = 0
		x = 620 
	end
	if ( y < 0 ) then 
		self.yVel = 0
		y = 0 
	end
	if ( y > 340 ) then 
		self.yVel = 0
		y = 340 
	end
	return x, y
end

function Character:Setup( options )
	self.name 			= options.name
	self.timerSpeed = options.timerSpeed
	self.speed 		= options.speed
	self.sound			= options.sound
	self.name			= options.name
	self.image			= Bitmap.new( options.texture )
	self.image:setPosition( self.RandomPosition() )
	self.image:setAnchorPoint( 0.5, 0.5 )
	self.image:setScale( 2, 2 )
	
	self.angle 			= self.RandomAngle()
	self.behavior 	= self.RandomBehavior()
end

function Character:Update()
	local x, y = self.image:getPosition()
	
	-- print( self.speed, self.angle, self.timer, self.name, self.behavior, x, y )
	
	self.timer = self.timer + self.timerSpeed
	local oldBehavior = self.behavior
	
	if ( self.timer >= 20 ) then
		-- Change behavior
		self.timer = 0
		self.behavior = self.RandomBehavior()
		self.angle = self.RandomAngle()
		self.image:setRotation( self.angle )
		print( self.name, self.behavior )
		
		local s = math.random( 0, 100 )
		if ( s > 98 ) then	self.sound:play() end
	end
	
	if ( self.behavior == "walk" ) then
		self:MoveForward( self.angle, self.speed )
	elseif ( self.behavior == "run" ) then
		self:MoveForward( self.angle, self.speed * 2 )
	else
		self:Deaccelerate()
	end
	
	x, y = self.image:getPosition()
	x, y = self:Physics( x, y )
	x, y = self:StayOnScreen( x, y )
	
	--if ( self.behavior == "disappear" ) then
		--self.image:setAlpha( 0 )
	--else
		--self.image:setAlpha( 1 )
	--end
	
	self.image:setPosition( x, y )
end